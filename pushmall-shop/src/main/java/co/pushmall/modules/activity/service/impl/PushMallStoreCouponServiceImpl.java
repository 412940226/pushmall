package co.pushmall.modules.activity.service.impl;

import co.pushmall.modules.activity.domain.PushMallStoreCoupon;
import co.pushmall.modules.activity.repository.PushMallStoreCouponRepository;
import co.pushmall.modules.activity.service.PushMallStoreCouponService;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponQueryCriteria;
import co.pushmall.modules.activity.service.mapper.PushMallStoreCouponMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-11-09
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStoreCouponServiceImpl implements PushMallStoreCouponService {

    private final PushMallStoreCouponRepository pushMallStoreCouponRepository;

    private final PushMallStoreCouponMapper pushMallStoreCouponMapper;

    public PushMallStoreCouponServiceImpl(PushMallStoreCouponRepository pushMallStoreCouponRepository, PushMallStoreCouponMapper pushMallStoreCouponMapper) {
        this.pushMallStoreCouponRepository = pushMallStoreCouponRepository;
        this.pushMallStoreCouponMapper = pushMallStoreCouponMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallStoreCouponQueryCriteria criteria, Pageable pageable) {
        Page<PushMallStoreCoupon> page = pushMallStoreCouponRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallStoreCouponMapper::toDto));
    }

    @Override
    public List<PushMallStoreCouponDTO> queryAll(PushMallStoreCouponQueryCriteria criteria) {
        return pushMallStoreCouponMapper.toDto(pushMallStoreCouponRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStoreCouponDTO findById(Integer id) {
        Optional<PushMallStoreCoupon> yxStoreCoupon = pushMallStoreCouponRepository.findById(id);
        ValidationUtil.isNull(yxStoreCoupon, "PushMallStoreCoupon", "id", id);
        return pushMallStoreCouponMapper.toDto(yxStoreCoupon.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStoreCouponDTO create(PushMallStoreCoupon resources) {
        return pushMallStoreCouponMapper.toDto(pushMallStoreCouponRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStoreCoupon resources) {
        Optional<PushMallStoreCoupon> optionalPushMallStoreCoupon = pushMallStoreCouponRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStoreCoupon, "PushMallStoreCoupon", "id", resources.getId());
        PushMallStoreCoupon pushMallStoreCoupon = optionalPushMallStoreCoupon.get();
        pushMallStoreCoupon.copy(resources);
        pushMallStoreCouponRepository.save(pushMallStoreCoupon);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallStoreCouponRepository.deleteById(id);
    }
}
