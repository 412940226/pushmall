package co.pushmall.modules.activity.rest;

import cn.hutool.core.util.ObjectUtil;
import co.pushmall.aop.log.Log;
import co.pushmall.modules.activity.domain.PushMallStoreSeckill;
import co.pushmall.modules.activity.service.PushMallStoreSeckillService;
import co.pushmall.modules.activity.service.dto.PushMallStoreSeckillQueryCriteria;
import co.pushmall.utils.OrderUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-12-14
 */
@Api(tags = "商城:秒杀管理")
@RestController
@RequestMapping("api")
public class StoreSeckillController {

    private final PushMallStoreSeckillService pushMallStoreSeckillService;

    public StoreSeckillController(PushMallStoreSeckillService pushMallStoreSeckillService) {
        this.pushMallStoreSeckillService = pushMallStoreSeckillService;
    }

    @Log("列表")
    @ApiOperation(value = "列表")
    @GetMapping(value = "/PmStoreSeckill")
    @PreAuthorize("@el.check('admin','YXSTORESECKILL_ALL','YXSTORESECKILL_SELECT')")
    public ResponseEntity getPushMallStoreSeckills(PushMallStoreSeckillQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallStoreSeckillService.queryAll(criteria, pageable), HttpStatus.OK);
    }


    @Log("发布")
    @ApiOperation(value = "发布")
    @PutMapping(value = "/PmStoreSeckill")
    @PreAuthorize("@el.check('admin','YXSTORESECKILL_ALL','YXSTORESECKILL_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallStoreSeckill resources) {
        if (ObjectUtil.isNotNull(resources.getStartTimeDate())) {
            resources.setStartTime(OrderUtil.
                    dateToTimestamp(resources.getStartTimeDate()));
        }
        if (ObjectUtil.isNotNull(resources.getEndTimeDate())) {
            resources.setStopTime(OrderUtil.
                    dateToTimestamp(resources.getEndTimeDate()));
        }
        if (ObjectUtil.isNull(resources.getId())) {
            resources.setAddTime(String.valueOf(OrderUtil.getSecondTimestampTwo()));
            return new ResponseEntity(pushMallStoreSeckillService.create(resources), HttpStatus.CREATED);
        } else {
            pushMallStoreSeckillService.update(resources);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @Log("删除")
    @ApiOperation(value = "删除")
    @DeleteMapping(value = "/PmStoreSeckill/{id}")
    @PreAuthorize("@el.check('admin','YXSTORESECKILL_ALL','YXSTORESECKILL_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallStoreSeckillService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
