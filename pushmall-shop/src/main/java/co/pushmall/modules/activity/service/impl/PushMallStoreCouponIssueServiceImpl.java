package co.pushmall.modules.activity.service.impl;

import co.pushmall.modules.activity.domain.PushMallStoreCouponIssue;
import co.pushmall.modules.activity.repository.PushMallStoreCouponIssueRepository;
import co.pushmall.modules.activity.service.PushMallStoreCouponIssueService;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponIssueDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponIssueQueryCriteria;
import co.pushmall.modules.activity.service.mapper.PushMallStoreCouponIssueMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-11-09
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStoreCouponIssueServiceImpl implements PushMallStoreCouponIssueService {

    private final PushMallStoreCouponIssueRepository pushMallStoreCouponIssueRepository;

    private final PushMallStoreCouponIssueMapper pushMallStoreCouponIssueMapper;

    public PushMallStoreCouponIssueServiceImpl(PushMallStoreCouponIssueRepository pushMallStoreCouponIssueRepository, PushMallStoreCouponIssueMapper pushMallStoreCouponIssueMapper) {
        this.pushMallStoreCouponIssueRepository = pushMallStoreCouponIssueRepository;
        this.pushMallStoreCouponIssueMapper = pushMallStoreCouponIssueMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallStoreCouponIssueQueryCriteria criteria, Pageable pageable) {
        Page<PushMallStoreCouponIssue> page = pushMallStoreCouponIssueRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallStoreCouponIssueMapper::toDto));
    }

    @Override
    public List<PushMallStoreCouponIssueDTO> queryAll(PushMallStoreCouponIssueQueryCriteria criteria) {
        return pushMallStoreCouponIssueMapper.toDto(pushMallStoreCouponIssueRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStoreCouponIssueDTO findById(Integer id) {
        Optional<PushMallStoreCouponIssue> yxStoreCouponIssue = pushMallStoreCouponIssueRepository.findById(id);
        ValidationUtil.isNull(yxStoreCouponIssue, "PushMallStoreCouponIssue", "id", id);
        return pushMallStoreCouponIssueMapper.toDto(yxStoreCouponIssue.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStoreCouponIssueDTO create(PushMallStoreCouponIssue resources) {
        return pushMallStoreCouponIssueMapper.toDto(pushMallStoreCouponIssueRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStoreCouponIssue resources) {
        Optional<PushMallStoreCouponIssue> optionalPushMallStoreCouponIssue = pushMallStoreCouponIssueRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStoreCouponIssue, "PushMallStoreCouponIssue", "id", resources.getId());
        PushMallStoreCouponIssue pushMallStoreCouponIssue = optionalPushMallStoreCouponIssue.get();
        pushMallStoreCouponIssue.copy(resources);
        pushMallStoreCouponIssueRepository.save(pushMallStoreCouponIssue);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallStoreCouponIssueRepository.deleteById(id);
    }
}
