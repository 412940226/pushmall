package co.pushmall.modules.shop.rest;

import co.pushmall.aop.log.Log;
import co.pushmall.modules.shop.domain.PushMallMaterialGroup;
import co.pushmall.modules.shop.service.PushMallMaterialGroupService;
import co.pushmall.modules.shop.service.dto.PushMallMaterialGroupQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2020-01-09
 */
@Api(tags = "商城:素材分组管理")
@RestController
@RequestMapping("/api/materialgroup")
public class MaterialGroupController {

    private final PushMallMaterialGroupService pushMallMaterialGroupService;

    public MaterialGroupController(PushMallMaterialGroupService pushMallMaterialGroupService) {
        this.pushMallMaterialGroupService = pushMallMaterialGroupService;
    }


    @GetMapping(value = "/page")
    @Log("查询素材分组")
    @ApiOperation("查询素材分组")
    public ResponseEntity<Object> getPushMallMaterialGroups(PushMallMaterialGroupQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity<>(pushMallMaterialGroupService.queryAll(criteria, pageable), HttpStatus.OK);
    }

    @PostMapping
    @Log("新增素材分组")
    @ApiOperation("新增素材分组")
    public ResponseEntity<Object> create(@Validated @RequestBody PushMallMaterialGroup resources) {
        return new ResponseEntity<>(pushMallMaterialGroupService.create(resources), HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改素材分组")
    @ApiOperation("修改素材分组")
    public ResponseEntity<Object> update(@Validated @RequestBody PushMallMaterialGroup resources) {
        pushMallMaterialGroupService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除素材分组")
    @ApiOperation("删除素材分组")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> deleteAll(@PathVariable String id) {
        pushMallMaterialGroupService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
