package co.pushmall.modules.shop.service.dto;

import co.pushmall.annotation.Query;
import lombok.Data;

/**
 * @author pushmall
 * @date 2019-10-18
 */
@Data
public class PushMallSystemGroupDataQueryCriteria {
    // 精确
    @Query
    private String groupName;
}
