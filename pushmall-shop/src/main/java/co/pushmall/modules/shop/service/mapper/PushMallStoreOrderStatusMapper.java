package co.pushmall.modules.shop.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.shop.domain.PushMallStoreOrderStatus;
import co.pushmall.modules.shop.service.dto.PushMallStoreOrderStatusDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-11-02
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallStoreOrderStatusMapper extends EntityMapper<PushMallStoreOrderStatusDTO, PushMallStoreOrderStatus> {

}
