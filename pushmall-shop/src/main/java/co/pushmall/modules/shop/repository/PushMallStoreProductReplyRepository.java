package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallStoreProductReply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-11-03
 */
public interface PushMallStoreProductReplyRepository extends JpaRepository<PushMallStoreProductReply, Integer>, JpaSpecificationExecutor {
}
