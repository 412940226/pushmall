package co.pushmall.modules.shop.service.impl;

import co.pushmall.exception.EntityExistException;
import co.pushmall.modules.shop.domain.PushMallUserRecharge;
import co.pushmall.modules.shop.repository.PushMallUserRechargeRepository;
import co.pushmall.modules.shop.service.PushMallUserRechargeService;
import co.pushmall.modules.shop.service.dto.PushMallUserRechargeDto;
import co.pushmall.modules.shop.service.dto.PushMallUserRechargeQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallUserRechargeMapper;
import co.pushmall.utils.FileUtil;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2020-03-02
 */
@Service
//@CacheConfig(cacheNames = "yxUserRecharge")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallUserRechargeServiceImpl implements PushMallUserRechargeService {

    private final PushMallUserRechargeRepository pushMallUserRechargeRepository;

    private final PushMallUserRechargeMapper pushMallUserRechargeMapper;

    public PushMallUserRechargeServiceImpl(PushMallUserRechargeRepository pushMallUserRechargeRepository, PushMallUserRechargeMapper pushMallUserRechargeMapper) {
        this.pushMallUserRechargeRepository = pushMallUserRechargeRepository;
        this.pushMallUserRechargeMapper = pushMallUserRechargeMapper;
    }

    @Override
    //@Cacheable
    public Map<String, Object> queryAll(PushMallUserRechargeQueryCriteria criteria, Pageable pageable) {
        Page<PushMallUserRecharge> page = pushMallUserRechargeRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallUserRechargeMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<PushMallUserRechargeDto> queryAll(PushMallUserRechargeQueryCriteria criteria) {
        return pushMallUserRechargeMapper.toDto(pushMallUserRechargeRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public PushMallUserRechargeDto findById(Integer id) {
        PushMallUserRecharge pushMallUserRecharge = pushMallUserRechargeRepository.findById(id).orElseGet(PushMallUserRecharge::new);
        ValidationUtil.isNull(pushMallUserRecharge.getId(), "PushMallUserRecharge", "id", id);
        return pushMallUserRechargeMapper.toDto(pushMallUserRecharge);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public PushMallUserRechargeDto create(PushMallUserRecharge resources) {
        if (pushMallUserRechargeRepository.findByOrderId(resources.getOrderId()) != null) {
            throw new EntityExistException(PushMallUserRecharge.class, "order_id", resources.getOrderId());
        }
        return pushMallUserRechargeMapper.toDto(pushMallUserRechargeRepository.save(resources));
    }


    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(Integer[] ids) {
        for (Integer id : ids) {
            pushMallUserRechargeRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<PushMallUserRechargeDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (PushMallUserRechargeDto yxUserRecharge : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("充值用户UID", yxUserRecharge.getUid());
            map.put("订单号", yxUserRecharge.getOrderId());
            map.put("充值金额", yxUserRecharge.getPrice());
            map.put("充值类型", yxUserRecharge.getRechargeType());
            map.put("是否充值", yxUserRecharge.getPaid());
            map.put("充值支付时间", yxUserRecharge.getPayTime());
            map.put("充值时间", yxUserRecharge.getAddTime());
            map.put("退款金额", yxUserRecharge.getRefundPrice());
            map.put("昵称", yxUserRecharge.getNickname());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}
