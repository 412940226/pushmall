package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallStoreOrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-11-02
 */
public interface PushMallStoreOrderStatusRepository extends JpaRepository<PushMallStoreOrderStatus, Integer>, JpaSpecificationExecutor {
}
