package co.pushmall.modules.shop.service.dto;

import co.pushmall.annotation.Query;
import lombok.Data;

/**
 * @author pushmall
 * @date 2020-03-22
 */
@Data
public class PushMallSystemStoreStaffQueryCriteria {

    /**
     * 模糊
     */
    @Query(type = Query.Type.INNER_LIKE)
    private String staffName;

    /**
     * 模糊
     */
    @Query(type = Query.Type.INNER_LIKE)
    private String nickname;

    @Query(type = Query.Type.EQUAL)
    private Integer uid;

    @Query(type = Query.Type.EQUAL)
    private Integer spreadUid;
}
